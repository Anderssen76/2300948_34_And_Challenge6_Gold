module.exports = (sequelize, Sequelize) => {
    const user_game_biodata = sequelize.define("user_game_biodata", {
      nama: {
        type: Sequelize.STRING
      },
      tempat_lahir: {
        type: Sequelize.STRING
      },
      umur: {
        type: Sequelize.STRING
      },
      published: {
        type: Sequelize.BOOLEAN
      }
    });
  
    return user_game_biodata;
  };