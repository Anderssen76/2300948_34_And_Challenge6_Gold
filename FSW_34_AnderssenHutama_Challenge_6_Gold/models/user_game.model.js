module.exports = (sequelize, Sequelize) => {
    const user_game = sequelize.define("user_game", {
      username: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      },
      published: {
        type: Sequelize.BOOLEAN
      }
    });
  
    return user_game;
  };