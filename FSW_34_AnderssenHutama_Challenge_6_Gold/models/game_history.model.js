module.exports = (sequelize, Sequelize) => {
    const game_history_biodata = sequelize.define("game_history_biodata", {
      waktu_bermain: {
        type: Sequelize.STRING
      },
      skor: {
        type: Sequelize.STRING
      },
      published: {
        type: Sequelize.BOOLEAN
      }
    });
  
    return game_history_biodata;
  };