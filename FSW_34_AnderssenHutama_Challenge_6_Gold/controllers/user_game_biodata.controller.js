const db = require("../models");
const User_game_biodata = db.user_games_biodata;
const Op = db.Sequelize.Op;

// Create and Save a new user_game_biodata
exports.create = (req, res) => {
    // Validate request
    if (!req.body.nama) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }
  
    // Create a user_game_biodata
    const user_game_biodata = {
      nama: req.body.nama,
      umur: req.body.umur,
      tempat_lahir: req.body.tempat_lahir,
      published: req.body.published ? req.body.published : false
    };
  
    // Save user_game_biodata in the database
    User_game_biodata.create(user_game_biodata)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "ERROR TIDAK BISA MEMBUAT ID."
        });
      });
  };
// Retrieve all user_game_biodatas from the database.
exports.findAll = (req, res) => {
    const nama = req.query.nama;
    var condition = nama ? { nama: { [Op.iLike]: `%${nama}%` } } : null;
  
    User_game_biodata.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "ERROR TIDAK BISA MENAMPILKAN SEMUA ID."
        });
      });
  };

// Find a single user_game_biodata with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
  
    User_game_biodata.findByPk(id)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).send({
            message: `TIDAK BISA MENEMUKAN ID=${id}.`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving user_game_biodata with id=" + id
        });
      });
  };

// Update a user_game_biodata by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
  
    User_game_biodata.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "ID was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update user_game_biodata with id=${id}. Maybe user_game_biodata was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating user_game_biodata with id=" + id
        });
      });
  };

// Delete a user_game_biodata with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
  
    User_game_biodata.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "ID TELAH TERHAPUS"
          });
        } else {
          res.send({
            message: `ID TIDAK DAPAT DIHAPUS id=${id}. ID TIDAK DITEMUKAN`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "TIDAK DAPAT MENGHAPUS ID=" + id
        });
      });
  };

// Delete all user_game_biodatas from the database.
exports.deleteAll = (req, res) => {
    User_game_biodata.destroy({
      where: {},
      truncate: false
    })
      .then(nums => {
        res.send({ message: `${nums} SEMUA ID TELAH TERHAPUS` });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all ID."
        });
      });
  };

// Find all published user_game_biodatas
exports.findAllPublished = (req, res) => {
    User_game_biodata.findAll({ where: { published: true } })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving ID."
        });
      });
  };