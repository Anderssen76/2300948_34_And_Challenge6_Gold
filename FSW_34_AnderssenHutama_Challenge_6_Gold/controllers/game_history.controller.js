const db = require("../models");
const Game_history = db.game_historys;
const Op = db.Sequelize.Op;

// Create and Save a new game_history
exports.create = (req, res) => {
    // Validate request
    if (!req.body.waktu_bermain) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }
  
    // Create a game_history
    const game_history = {
      waktu_bermain: req.body.waktu_bermain,
      skor: req.body.skor,
      published: req.body.published ? req.body.published : false
    };
  
    // Save game_history in the database
    Game_history.create(game_history)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "ERROR TIDAK BISA MEMBUAT ID."
        });
      });
  };
// Retrieve all game_historys from the database.
exports.findAll = (req, res) => {
    const waktu_bermain = req.query.waktu_bermain;
    var condition = waktu_bermain ? { waktu_bermain: { [Op.iLike]: `%${waktu_bermain}%` } } : null;
  
    Game_history.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "ERROR TIDAK BISA MENAMPILKAN SEMUA ID."
        });
      });
  };

// Find a single game_history with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
  
    Game_history.findByPk(id)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).send({
            message: `TIDAK BISA MENEMUKAN ID=${id}.`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving game_history with id=" + id
        });
      });
  };

// Update a game_history by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
  
    Game_history.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "ID was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update game_history with id=${id}. Maybe game_history was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating game_history with id=" + id
        });
      });
  };

// Delete a game_history with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
  
    Game_history.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "ID TELAH TERHAPUS"
          });
        } else {
          res.send({
            message: `ID TIDAK DAPAT DIHAPUS id=${id}. ID TIDAK DITEMUKAN`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "TIDAK DAPAT MENGHAPUS ID=" + id
        });
      });
  };

// Delete all game_historys from the database.
exports.deleteAll = (req, res) => {
    Game_history.destroy({
      where: {},
      truncate: false
    })
      .then(nums => {
        res.send({ message: `${nums} SEMUA ID TELAH TERHAPUS` });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all ID."
        });
      });
  };

// Find all published game_historys
exports.findAllPublished = (req, res) => {
    Game_history.findAll({ where: { published: true } })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving ID."
        });
      });
  };