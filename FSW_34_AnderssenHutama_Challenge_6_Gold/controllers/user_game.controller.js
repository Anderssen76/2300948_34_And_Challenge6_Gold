const db = require("../models");
const User_game = db.user_games;
const Op = db.Sequelize.Op;

// Create and Save a new user_game
exports.create = (req, res) => {
    // Validate request
    if (!req.body.username) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }
  
    // Create a user_game
    const user_game = {
      username: req.body.username,
      password: req.body.password,
      published: req.body.published ? req.body.published : false
    };
  
    // Save user_game in the database
    User_game.create(user_game)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "ERROR TIDAK BISA MEMBUAT ID."
        });
      });
  };
// Retrieve all user_games from the database.
exports.findAll = (req, res) => {
    const username = req.query.username;
    var condition = username ? { username: { [Op.iLike]: `%${username}%` } } : null;
  
    User_game.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "ERROR TIDAK BISA MENAMPILKAN SEMUA ID."
        });
      });
  };

// Find a single user_game with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
  
    User_game.findByPk(id)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).send({
            message: `TIDAK BISA MENEMUKAN ID=${id}.`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving user_game with id=" + id
        });
      });
  };

// Update a user_game by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
  
    User_game.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "ID was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update user_game with id=${id}. Maybe user_game was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating user_game with id=" + id
        });
      });
  };

// Delete a user_game with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
  
    User_game.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "ID TELAH TERHAPUS"
          });
        } else {
          res.send({
            message: `ID TIDAK DAPAT DIHAPUS id=${id}. ID TIDAK DITEMUKAN`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "TIDAK DAPAT MENGHAPUS ID=" + id
        });
      });
  };

// Delete all user_games from the database.
exports.deleteAll = (req, res) => {
    User_game.destroy({
      where: {},
      truncate: false
    })
      .then(nums => {
        res.send({ message: `${nums} SEMUA ID TELAH TERHAPUS` });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all ID."
        });
      });
  };

// Find all published user_games
exports.findAllPublished = (req, res) => {
    User_game.findAll({ where: { published: true } })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving ID."
        });
      });
  };