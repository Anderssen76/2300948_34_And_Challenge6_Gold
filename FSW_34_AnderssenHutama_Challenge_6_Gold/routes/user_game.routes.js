module.exports = app => {
    const user_games = require("../controllers/user_game.controller.js");
  
    var router = require("express").Router();
  
    // Create a new user_game
    router.post("/", user_games.create);
  
    // Retrieve all user_games
    router.get("/", user_games.findAll);
  
    // Retrieve all published user_games
    router.get("/published", user_games.findAllPublished);
  
    // Retrieve a single user_game with id
    router.get("/:id", user_games.findOne);
  
    // Update a user_game with id
    router.put("/:id", user_games.update);
  
    // Delete a user_game with id
    router.delete("/:id", user_games.delete);
  
    // Create a new user_game
    router.delete("/", user_games.deleteAll);
  
    app.use('/api/user_games', router);
  };
  