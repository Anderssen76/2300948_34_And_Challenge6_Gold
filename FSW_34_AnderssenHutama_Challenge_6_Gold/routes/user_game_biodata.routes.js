module.exports = app => {
    const user_games_biodata = require("../controllers/user_game_biodata.controller.js");
  
    var router = require("express").Router();
  
    // Create a new user_game
    router.post("/", user_games_biodata.create);
  
    // Retrieve all user_games_biodata
    router.get("/", user_games_biodata.findAll);
  
    // Retrieve all published user_games_biodata
    router.get("/published", user_games_biodata.findAllPublished);
  
    // Retrieve a single user_game with id
    router.get("/:id", user_games_biodata.findOne);
  
    // Update a user_game with id
    router.put("/:id", user_games_biodata.update);
  
    // Delete a user_game with id
    router.delete("/:id", user_games_biodata.delete);
  
    // Create a new user_game
    router.delete("/", user_games_biodata.deleteAll);
  
    app.use('/api/user_games_biodata', router);
  };
  