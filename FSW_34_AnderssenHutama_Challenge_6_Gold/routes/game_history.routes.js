module.exports = app => {
    const game_historys = require("../controllers/game_history.controller.js");
  
    var router = require("express").Router();
  
    // Create a new user_game
    router.post("/", game_historys.create);
  
    // Retrieve all game_historys
    router.get("/", game_historys.findAll);
  
    // Retrieve all published game_historys
    router.get("/published", game_historys.findAllPublished);
  
    // Retrieve a single user_game with id
    router.get("/:id", game_historys.findOne);
  
    // Update a user_game with id
    router.put("/:id", game_historys.update);
  
    // Delete a user_game with id
    router.delete("/:id", game_historys.delete);
  
    // Create a new user_game
    router.delete("/", game_historys.deleteAll);
  
    app.use('/api/game_historys', router);
  };
  