if (process.env.NODE_ENV !== "producion") {
    require("dotenv").config()
}

const express = require("express")
const app = express()
const bodyParser = require("body-parser")
const bcrypt = require("bcrypt")
const passport = require("passport")
const initializePassport = require("./passport-config")
const flash = require("express-flash")
const session = require("express-session")


const cors = require("cors");

var corsOptions = {
    origin: "http://localhost:8081"
  };
  
  app.use(cors(corsOptions));


  const db = require("./models");
  db.sequelize.sync({force:true})
    .then(() => {
      console.log("Synced db.");
    })
    .catch((err) => {
      console.log("Failed to sync db: " + err.message);
    });
  
    app.use(express.json());


initializePassport(
    passport,
    email => users.find (user => user.email === email),
    id => users.find (user => user.id === id )
)

app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("public"));
app.use(flash())
app.use(session({
    secret: process.env.SECRET_KEY,
    resave: false, 
    saveUninitialized: false
}))
app.use(passport.initialize())
app.use(passport.session())


const users = []

app.post("/login", passport.authenticate("local", {
    successRedirect: "/dashboard",
    failureRedirect: "/login", 
    failureFlash: true
}))

app.post("/register", async (req, res) => {
    try {
        const hashedPassword = await bcrypt.hash(req.body.password, 10)
        users.push({
            id: Date.now().toString(),
            name: req.body.name,
            email: req.body.email,
            password: hashedPassword,
        })
        console.log(users);
        res.redirect("/login")
    } catch (e) {
        console.log(e);
        res.redirect("/register")
    }
})


app.get('/', (req, res) => {
    res.render("index.ejs")
})
app.get('/login', (req, res) => {
    res.render("login.ejs")
})
app.get('/register', (req, res) => {
    res.render("register.ejs")
})
app.get('/game', (req, res) => {
    res.render("game.ejs")
})
app.get('/dashboard', (req, res) => {
    res.render("dashboard.ejs", {name: req.user.name})
})


require("./routes/user_game.routes.js")(app);           //http://localhost:8000/api/user_games/
require("./routes/user_game_biodata.routes.js")(app);   //http://localhost:8000/api/user_games_biodata/
require("./routes/game_history.routes.js")(app);        //http://localhost:8000/api/game_historys/
// set port, listen for requests
const PORT = process.env.PORT || 8000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
